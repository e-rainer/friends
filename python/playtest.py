import vlc
import time

p = vlc.MediaPlayer("sounds/hello.wav")

def onEnd(event):
    if event.type == vlc.EventType.MediaPlayerEndReached:
        print("End of file, rewind")
        p.set_media(p.get_media())
        p.play()

em = p.event_manager()
em.event_attach(vlc.EventType.MediaPlayerEndReached, onEnd)
p.play()

while True:
    print("loop")
    time.sleep(10)
    