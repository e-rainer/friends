FROM   ros:melodic

RUN     apt update
RUN     apt upgrade -y
RUN     apt install -y python3-pip curl vim mc less python3-empy ros-melodic-sound-play
RUN     pip3 install catkin_pkg pyyaml

WORKDIR /friends_ws
RUN     mkdir src
RUN     . /opt/ros/melodic/setup.sh && catkin_make -DPYTHON_EXECUTABLE=/usr/bin/python3

RUN     curl https://gitlab.com/e-rainer/face/-/archive/master/face-master.tar | tar xvf - face-master/face
RUN     mv face-master/face src/
RUN     rm -rf face-master

ADD     friends /friends_ws/src/friends

RUN     . /opt/ros/melodic/setup.sh && catkin_make
RUN     . /opt/ros/melodic/setup.sh && catkin_make install
