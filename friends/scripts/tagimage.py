import cv2

cam         = cv2.VideoCapture(0)
faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

for i in range(2):

    ret, frame = cam.read()

    if not ret:
        print("failed to grab frame")
        break

    gray  = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30)
    )

    print("Found {0} faces!".format(len(faces)))

    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

    cv2.imwrite("opencv_frame_{}.png".format(i), frame)
    cv2.imshow("Faces found", frame)
    cv2.waitKey(0)

cam.release()
cv2.destroyAllWindows()
