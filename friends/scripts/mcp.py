#!/usr/bin/env python
import rospy
from   std_msgs.msg import String
from   face.msg     import Servo_Array
from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient

nodding = -100

def talker():
    global nodding
    pub = rospy.Publisher('erainer_face', Servo_Array, queue_size=10)
    rospy.init_node('friend', anonymous=True)

    soundhandle = SoundClient()
    rospy.sleep(1)

    soundhandle.say("Hello there. It's me, Rainer. I am happy to see you.", 'voice_us2_mbrola', 1.0)
    rospy.sleep(1)

    rate = rospy.Rate(10)

    while not rospy.is_shutdown():
        face = Servo_Array()
        face.nodding = nodding
        nodding += 1

        if nodding > 100:
            nodding = -100
            soundhandle.playWave('snoring.wav')

        pub.publish(face)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
