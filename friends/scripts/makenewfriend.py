from PIL import Image
import cv2
import time
import face_recognition
from playsound import playsound


FRIEND = "ansi"
cam    = cv2.VideoCapture(0)

ansi_image         = face_recognition.load_image_file("friends/ansi/038.png")
ansi_face_encoding = face_recognition.face_encodings(ansi_image)[0]
known_faces        = [ansi_face_encoding]
counter = 10

while True:
    time.sleep(1)
    ret, frame = cam.read()

    if counter > 0:
        counter -= 1

    if not ret:
        print("failed to grab frame")
        break

    face_locations      = face_recognition.face_locations(frame)
    face_landmarks_list = face_recognition.face_landmarks(frame)

    print("I found {} face(s) in this photograph.".format(len(face_locations)))
    # print(face_landmarks_list)

    for face_location in face_locations:
        face_encoding = face_recognition.face_encodings(frame)[0]
        results = face_recognition.compare_faces(known_faces, face_encoding)
        print(results)

        if counter == 0:
          playsound("sounds/short_slightly.mp3")

          if len(results) > 0 and results[0] == True:
              playsound("sounds/helloansi.wav")
          else:
              playsound("sounds/hello.wav")
          counter = 5

        top, right, bottom, left = face_location
        face_image = frame[top:bottom, left:right]
        #pil_image = Image.fromarray(face_image)
        #pil_image.save("friends/%03d.png" % i)

cam.release()
